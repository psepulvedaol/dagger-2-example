package cl.ionix.dagger

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject @field:Choose(Constants.DAGGER_INFO) lateinit var daggerInfo : Info
    @Inject @field:Choose(Constants.WORLD_INFO) lateinit var worldInfo : Info

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DaggerMyComponent.create().inject(this)

        findViewById<TextView>(R.id.daggerTextView).text = "${daggerInfo.text}\n${worldInfo.text}"
    }
}
