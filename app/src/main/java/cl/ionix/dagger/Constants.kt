package cl.ionix.dagger

class Constants {
    companion object {
        const val DAGGER_INFO = "Dagger"
        const val WORLD_INFO = "World"
    }
}