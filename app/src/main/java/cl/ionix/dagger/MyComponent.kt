package cl.ionix.dagger

import dagger.Component

@Component(modules = [InfoModule::class])
interface MyComponent {
    fun inject(view : MainActivity)
}