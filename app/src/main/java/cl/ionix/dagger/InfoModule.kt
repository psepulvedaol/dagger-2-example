package cl.ionix.dagger

import dagger.Module
import dagger.Provides

@Module
open class InfoModule {

    @Provides @Choose(Constants.DAGGER_INFO)
    open fun providesDaggerInfo() : Info {
        return Info("Hello Dagger!")
    }

    @Provides @Choose(Constants.WORLD_INFO)
    open fun providesDaggerWorld() : Info {
        return Info("Hello World!")
    }

}