package cl.ionix.dagger

import junit.framework.TestCase.assertEquals
import org.junit.Test

import org.junit.Before
import javax.inject.Inject

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TestMainActivity {
    @Inject
    @field:Choose(Constants.WORLD_INFO) lateinit var infoWorld: Info
    @Inject @field:Choose(Constants.DAGGER_INFO) lateinit var infoDagger: Info

    @Before
    fun setup() {
        DaggerTestInfoComponent.builder().infoModule(TestInfoModule()).build().inject(this)
    }

    @Test
    fun simpleTest() {
        assertEquals("Hello World!", infoWorld.text)
        assertEquals("Hello Dagger!", infoDagger.text)
    }
}
