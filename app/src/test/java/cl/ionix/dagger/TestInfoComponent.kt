package cl.ionix.dagger

import dagger.Component

@Component(modules = [InfoModule::class])
interface TestInfoComponent : MyComponent {
    fun inject(app: TestMainActivity)
}