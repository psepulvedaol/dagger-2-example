package cl.ionix.dagger

class TestInfoModule : InfoModule() {

    override fun providesDaggerInfo() : Info {
        return Info("Hello Dagger!")
    }

}